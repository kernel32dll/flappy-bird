package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * class Bird
 * creates a bird object
 * her physics and so on
 */
public class Bird {
    Texture img;
    Vector2 position;
    float vy;
    float gravity;

    /**
     * public Bird
     * creates a bird object
     * origin of its coordinates
     */
    public Bird(){
        img = new Texture("pok1.png");
        position = new Vector2(100,380);
        vy = 0;
        gravity = - 0.7f;
    }
    /**
     * render
     *
     */
    public void render(SpriteBatch batch){
        batch.draw(img, position.x, position.y);
    }

    public void update(){

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            vy = 10;
        }
        vy += gravity;
        position.y += vy;
    }

    public void recreate(){
        position = new Vector2(100,380);
        vy = 0;
    }
}
