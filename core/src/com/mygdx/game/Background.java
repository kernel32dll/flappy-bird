package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Class Background
 * Create Background from game
 * Vectors, Positions etc
 *
 */

public class Background {

    class BGPicture{
        private Texture tx;
        private Vector2 pos;

        public BGPicture(Vector2 pos){
            tx = new Texture("space1.jpg");
            this.pos = pos;
        }

    }

    private int speed;
    private BGPicture[] backs;
    /**
     * int speed;
     * sets the speed of the picture
     */
    public Background(){
        speed = 4;
        backs = new BGPicture[2];
        backs[0] = new BGPicture(new Vector2(0,0));
        backs[1] = new BGPicture(new Vector2(800,0));
    }
    /**
     *  void render
     * sets the speed of the render (60 fps)
     */
    public void render(SpriteBatch batch){
        for (int i = 0; i < backs.length; i++) {
            batch.draw(backs[i].tx, backs[i].pos.x, backs[i].pos.y);
        }
    }
    /**
     *  void update
     * update picture
     * refresh image from excess garbage
     */
    public void update(){
        for (int i = 0; i < backs.length; i++) {
            backs[i].pos.x -= speed;
        }

        if(backs[0].pos.x < -800){
            backs[0].pos.x = 0;
            backs[1].pos.x = 800;
        }
    }
}
